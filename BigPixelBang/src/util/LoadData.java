package util;

import game.objects.Chunk;
import game.objects.blocks.Block;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;


import com.google.gson.Gson;

import world.World;

public class LoadData {

		public LoadData(){
			loadMaterials();
			
		}

		private void loadMaterials() {
			
			File file = new File("data/blocks/");
			String[] names =  file.list();
			
			for (String name : names) {
				World.addBlock(loadBlockJson(name, "blocks"));
			}
			
		}

		public static Block loadBlockJson(String json, String path) {
			
			Gson g = new Gson();
			
			try {
				
				BufferedReader buff = new BufferedReader(new FileReader("data/"+path+"/"+json));
				
				Block block = g.fromJson(buff, Block.class);
				block.setTextures();
				
				Log.show(block.getMaterial().toString());
				
				return block;
				
			} catch (Exception e) {
				Log.show("Exception : "+ e.getMessage());
			}
			
			return null;
		}
		
		public static Texture loadTextureJson(String json) {

			Gson g = new Gson();
			
			try {

				BufferedReader buff = new BufferedReader(new FileReader("data/resources/data/"+json));

				Texture text = g.fromJson(buff, Texture.class);

				return text;

			} catch (Exception e) {
				Log.show("Exception : "+ e.getMessage());
			}

			return null;
		}
		
		
		public static Chunk loadChunkJson(String json, String path) {

			Gson g = new Gson();
			
			try {

				BufferedReader buff = new BufferedReader(new FileReader("data/"+path+"/"+json));
				Chunk chunk = g.fromJson(buff, Chunk.class);

				Log.show("Chunk load");

				return chunk;

			} catch (Exception e) {
				Log.show("Exception : "+ e.getMessage());
			}

			return null;
		}
}
