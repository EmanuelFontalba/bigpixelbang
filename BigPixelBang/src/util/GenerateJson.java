package util;

import static org.lwjgl.opengl.GL11.*;
import java.io.FileWriter;
import java.io.IOException;

import com.google.gson.Gson;

import game.objects.Chunk;
import game.window.DefaultValues;
import util.Texture;

public class GenerateJson {

	public static void chunkJson() {
	
		Chunk obj = new Chunk(DefaultValues.WIDTH/2-32, DefaultValues.HEIGHT/4*3);
		
		Gson gson = new Gson();
	 
		// convert java object to JSON format,
		// and returned as JSON formatted string
		String json = gson.toJson(obj);
	 
		try {
			//write converted json data to a file named "file.json"
			FileWriter writer = new FileWriter("data/world/world.json");
			writer.write(json);
			writer.close();
	 
		} catch (IOException e) {
			e.printStackTrace();
		}
	 
		System.out.println(json);
	 
	    }
	
	public static void texturesJson(String path,String name){
		
		Texture obj = Texture.loadTexture(path);
		Gson gson = new Gson();
	 
		// convert java object to JSON format,
		// and returned as JSON formatted string
		String json = gson.toJson(obj);
	 
		try {
			//write converted json data to a file named "file.json"
			FileWriter writer = new FileWriter("data/resources/data/"+name+".json");
			writer.write(json);
			writer.close();
	 
		} catch (IOException e) {
			e.printStackTrace();
		}
	 
		System.out.println(json);
	 
	    
		
	}
	
}
