package world;

import java.util.ArrayList;

import util.GenerateJson;
import util.LoadData;
import game.objects.Chunk;
import game.objects.blocks.Block;
import game.textures.Textures;
import game.window.DefaultValues;
import game.objects.Character;

public class World {
	
	public static ArrayList<Block> blocks = new ArrayList<Block>();
	
	public static ArrayList<Chunk> chunks = new ArrayList<Chunk>();
	
	public static Character character = new Character(DefaultValues.WIDTH/2, DefaultValues.HEIGHT/2);

	public static void addBlock(Block block){
		blocks.add(block);
	}
	
	public static void generateWorld(){
		Textures.generateTextures();
		GenerateJson.chunkJson();
		
		chunks.add(LoadData.loadChunkJson("world.json", "world"));
		
	}
	
	public static void renderWorld(){
		
		for (Chunk chunk : chunks) {
			chunk.render();
		}	
		
		character.render();
		
	}
}
