package game.objects;

import static org.lwjgl.opengl.GL11.GL_QUADS;
import static org.lwjgl.opengl.GL11.glBegin;
import static org.lwjgl.opengl.GL11.glEnd;
import static org.lwjgl.opengl.GL11.glTexCoord2d;
import static org.lwjgl.opengl.GL11.glVertex2d;
import game.textures.Textures;
import util.Texture;

public class Character {
	
	public double x;
	public double y;
	public double velocity = 2;
	
	//texturas cuerpo
	private Texture texture = Textures.loadTexture("gamer.json");
	private Texture hair = Textures.loadTexture("hair.json");
	
	//texturas del equipo
	
	
	//CONSTRUCT
	public Character(double x, double y){
		
		setX(x);
		setY(y);
		
	}
	
	public void render(){
		
		texture.bind();
		glBegin(GL_QUADS);
			glTexCoord2d(0, 0);
			glVertex2d(x, y);
			glTexCoord2d(0, 1);
			glVertex2d(x, y-81);
			glTexCoord2d(1, 1);
			glVertex2d(x+34, y-81);
			glTexCoord2d(1, 0);
			glVertex2d(x+34, y);
		glEnd();
		
		hair.bind();
		glBegin(GL_QUADS);
			glTexCoord2d(0, 0);
			glVertex2d(x+4, y);
			glTexCoord2d(0, 1);
			glVertex2d(x+4, y-29);
			glTexCoord2d(1, 1);
			glVertex2d(x+4+29, y-29);
			glTexCoord2d(1, 0);
			glVertex2d(x+4+29, y);
		glEnd();
		
	}
	
	
	
	
	
	
	//GETTERS & SETTERS

	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}

	
}
