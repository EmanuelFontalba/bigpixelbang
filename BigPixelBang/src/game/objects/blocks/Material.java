package game.objects.blocks;

public enum Material {
	
	NULL,
	DEFAULT,
	DIRT, 
	WOOD,
	GRASS;

}
