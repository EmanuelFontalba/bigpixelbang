package game.objects.blocks;

import static org.lwjgl.opengl.GL11.GL_QUADS;
import static org.lwjgl.opengl.GL11.glBegin;
import static org.lwjgl.opengl.GL11.glEnd;
import static org.lwjgl.opengl.GL11.glTexCoord2d;
import static org.lwjgl.opengl.GL11.glVertex2d;
import game.textures.Textures;
import util.Texture;

public class Block {
	
	public double x;
	public double y;
	
	private Texture up;
	private Texture left;
	private Texture right;
	
	private Material material;
	
	/**
	 * [0]->Render Up
	 * [1]->Render left
	 * [2]->Render right
	 */
	public boolean[] render = new boolean[3];
	
	//CONSTRUCT
	public Block(double x, double y, Material material, boolean[] render){
		
		setX(x);
		setY(y);
		setMaterial(material);
		setTextures();
		setRender(render);
		
		//setUp(Texture.loadTexture(DefaultValues.PATH_TEXTURE+material.toString()+"/UP.png"));
		//setLeft(Texture.loadTexture(DefaultValues.PATH_TEXTURE+material.toString()+"/LEFT.png"));
		//setRight(Texture.loadTexture(DefaultValues.PATH_TEXTURE+material.toString()+"/RIGHT.png"));
		
	}
	
	public void setRender(boolean [] render){
		this.render=render;
	}
	
	public void render(){
		if(material != Material.NULL){
		
			if(render[0])
				renderUp();
			if(render[1])
				renderLeft();
			if(render[2])
				renderRight();
		
		}
		
	}
	
	public void setTextures(){
		switch (material) {
		case DIRT:
			setUp(Textures.loadTexture("dirtUp.json"));
			setLeft(Textures.loadTexture("dirtLeft.json"));
			setRight(Textures.loadTexture("dirtRight.json"));
			break;
		case GRASS:
			setUp(Textures.loadTexture("grassUp.json"));
			setLeft(Textures.loadTexture("grassLeft.json"));
			setRight(Textures.loadTexture("grassRight.json"));
			break;
		case WOOD:
			setUp(Textures.loadTexture("woodUp.json"));
			setLeft(Textures.loadTexture("woodLeft.json"));
			setRight(Textures.loadTexture("woodRight.json"));
			break;
		case NULL:
			break;

		default:
			break;
		}
	}
	
	

	public void renderUp(){
		up.bind();
		glBegin(GL_QUADS);
			glTexCoord2d(0, 0);
			glVertex2d(x, y);
			glTexCoord2d(0, 1);
			glVertex2d(x, y-32);
			glTexCoord2d(1, 1);
			glVertex2d(x+64, y-32);
			glTexCoord2d(1, 0);
			glVertex2d(x+64, y);
		glEnd();
	}
	
	public void renderLeft(){
		left.bind();
		glBegin(GL_QUADS);
			glTexCoord2d(0, 0);
			glVertex2d(x, y-16);
			glTexCoord2d(0, 1);
			glVertex2d(x, y-16-48);
			glTexCoord2d(1, 1);
			glVertex2d(x+32, y-16-48);
			glTexCoord2d(1, 0);
			glVertex2d(x+32, y-16);
		glEnd();
	}
	
	public void renderRight(){
		right.bind();
		glBegin(GL_QUADS);
			glTexCoord2d(0, 0);
			glVertex2d(x+32, y-16);
			glTexCoord2d(0, 1);
			glVertex2d(x+32, y-16-48);
			glTexCoord2d(1, 1);
			glVertex2d(x+32+32, y-16-48);
			glTexCoord2d(1, 0);
			glVertex2d(x+32+32, y-16);
		glEnd();
	}
	
	
	
	
	//GETTERS & SETTERS

	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}

	public Texture getUp() {
		return up;
	}

	public void setUp(Texture up) {
		this.up = up;
	}

	public Texture getLeft() {
		return left;
	}

	public void setLeft(Texture left) {
		this.left = left;
	}

	public Texture getRight() {
		return right;
	}

	public void setRight(Texture right) {
		this.right = right;
	}

	public Material getMaterial() {
		return material;
	}

	public void setMaterial(Material material) {
		this.material = material;
	}

}
