package game.objects;


import static org.lwjgl.opengl.GL11.*;
import game.objects.blocks.Block;
import game.objects.blocks.Material;
import game.window.DefaultValues;

public class Chunk {
	
	//private ArrayList<Block> chunk = new ArrayList<Block>();
	private int xMax = 16;
	private int yMax = 16;
	private int zMax = 3;
	
	private Block[][][] chunk = new Block[xMax][yMax][zMax+5];
	
	
	private double xInit;
	private double yInit;

	
	//private int index =0;
	
	public Chunk(double xInit, double yInit){
		
		this.xInit=xInit;
		this.yInit=yInit;
		
		
		init();
		initTextures();
		
		
	
	}

	private void init() {
		
		double incrementX=0;
		double incrementY=0;
		
		double incrementWidth=1;
		double incrementHeight=0;
		
		Material materialDefault = Material.DIRT;
		Material material;
		
		//int random;
		
		boolean [] render = new boolean[]{true, true, true};
		
		for(int z=0; z<zMax; z++){
			
			for(int y=0; y<yMax; y++){
				
				for(int x=0; x<xMax; x++){
					
					//random=(int) (Math.random()*100);
					
//					if(random<20){
//						material=Material.NULL;
//					}
//					else{
//						material=materialDefault;
//					}
				
					if(z==zMax-1)
						material=Material.GRASS;
					else
						material=materialDefault;
					chunk[x][y][z]=new Block(this.xInit+incrementX, this.yInit+incrementY+incrementHeight, material, render);
					
//					chunk[x][y][z]=LoadData.loadBlockJson("dirt.json", "blocks");
//					chunk[x][y][z].setX(this.xInit+incrementX);
//					chunk[x][y][z].setY(this.yInit+incrementY+incrementHeight);
					
					
					
					//chunk.add(new Block(this.xInit+incrementX, this.yInit+incrementY+incrementHeight, material, render));
					//this.index++;
					incrementX-=32;
					incrementY-=16;
				
				}
				
				incrementX=0;
				incrementY=0;
				
				incrementX+=(32*incrementWidth);
				incrementY-=(16*incrementWidth);
				
				incrementWidth++;
			
			}
			
			incrementX=0;
			incrementY=0;
			
			incrementWidth=1;
			incrementHeight+=32;
		
		}
		
		boolean[][] wood = new boolean[xMax][yMax];
		
		for(int z = zMax; z<(zMax+5); z++){
			for(int y=0; y<yMax; y++){

				for(int x=0; x<xMax; x++){
					
					if(z==zMax){
						int random=(int) (Math.random()*100);
						
						if(random<5){
							wood[x][y]=true;
						}
						else{
							wood[x][y]=false;
						}
					}
					
					if(wood[x][y])
						chunk[x][y][z]=new Block(this.xInit+incrementX, this.yInit+incrementY+incrementHeight, Material.WOOD, render);
					else
						chunk[x][y][z]=null;
					
					incrementX-=32;
					incrementY-=16;

				}

				incrementX=0;
				incrementY=0;

				incrementX+=(32*incrementWidth);
				incrementY-=(16*incrementWidth);

				incrementWidth++;

			}
			
			incrementX=0;
			incrementY=0;
			
			incrementWidth=1;
			incrementHeight+=32;
		}
	}
	
	public void initTextures(){
		

		
		for(int z=0; z<zMax; z++){

			for(int y=0; y<yMax; y++){

				for(int x=0; x<xMax; x++){
					

					if(x==xMax-1 || y==yMax-1 || z == zMax-1)
							chunk[x][y][z].setRender(new boolean[]{true, true, true});
						
					else{
						chunk[x][y][z].setRender(new boolean[]{false, false, false});
					}
					
					
					
					
				}



			}

		}

		
	}
	
	
	public void render(){
		
		
		
		for(int z=0; z<zMax; z++){

			for(int y=0; y<yMax; y++){

				for(int x=0; x<xMax; x++){

						chunk[x][y][z].render();


				}



			}

		}
		
		glTranslated(0, 0, 1);
			
		
		for(int z=0; z<(zMax+5); z++){

			for(int y=0; y<yMax; y++){

				for(int x=0; x<xMax; x++){

					if(chunk[x][y][z]!=null)
						chunk[x][y][z].render();


				}



			}

		}
		
		glTranslated(0, 0, -1);
		
		
		
		
	}

}


