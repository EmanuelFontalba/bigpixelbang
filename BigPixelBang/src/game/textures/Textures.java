package game.textures;

import util.GenerateJson;
import util.LoadData;
import util.Texture;

public class Textures {

	public static void generateTextures() {
		GenerateJson.texturesJson("data/resources/textures/grass/UP.png", "grassUp");
		GenerateJson.texturesJson("data/resources/textures/grass/LEFT.png", "grassLeft");
		GenerateJson.texturesJson("data/resources/textures/grass/RIGHT.png", "grassRight");
		
		GenerateJson.texturesJson("data/resources/textures/dirt/UP.png", "dirtUp");
		GenerateJson.texturesJson("data/resources/textures/dirt/LEFT.png", "dirtLeft");
		GenerateJson.texturesJson("data/resources/textures/dirt/RIGHT.png", "dirtRight");
		
		GenerateJson.texturesJson("data/resources/textures/wood/UP.png", "woodUp");
		GenerateJson.texturesJson("data/resources/textures/wood/LEFT.png", "woodLeft");
		GenerateJson.texturesJson("data/resources/textures/wood/RIGHT.png", "woodRight");
		
		GenerateJson.texturesJson("data/resources/textures/buttonStart/BUTTON.png", "button");
		
		GenerateJson.texturesJson("data/resources/textures/gamer/DEFAULT.png", "gamer");
		GenerateJson.texturesJson("data/resources/textures/gamer/PELO.png", "hair");
	}
	
	public static Texture loadTexture(String json){
		return LoadData.loadTextureJson(json);
	}
	
}
