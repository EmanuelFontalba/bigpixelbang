package game.window;

import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.system.MemoryUtil.NULL;
import game.Input;
import game.textures.Textures;
import game.window.scenesLoops.GameLoop;
import game.window.scenesLoops.MenuLoop;

import java.nio.ByteBuffer;

import org.lwjgl.glfw.GLFWvidmode;
import org.lwjgl.opengl.GLContext;

import util.Log;
import util.Texture;
import world.World;



public class BigPixelBang {
	
	public static void main(String [] args){
		
		
		try{
			Log.show("Starting window.");
			init();
	
			loop();
	
		}finally{
			
			Log.show("Destroy the window");
			glfwDestroyWindow(DefaultValues.window);
			Log.show("Bye!");
			System.exit(0);
		}
	}
	
	private static void loop(){
		Log.show("Starting window game.");
		
		// Set the clear color
        glClearColor(1.0f, 1.0f, 0.9f, 1.0f);
		
		
		
		while(glfwWindowShouldClose(DefaultValues.window) == GL_FALSE){
			switch(DefaultValues.scene){
			case GAME:
				GameLoop.start();
				break;
			case MENU:
				MenuLoop.start();
				break;
			default:
				break;
			}
		}
	}
	
	/**
	 * Init the window.
	 */
	private static void init() {
		
		glfwInit();

		glfwDefaultWindowHints();
		glfwWindowHint(GLFW_RESIZABLE, GL_TRUE);
		
		
		
        
//        DefaultValues.window = glfwCreateWindow(GLFWvidmode.width(DefaultValues.vidmode)-16, GLFWvidmode.height(DefaultValues.vidmode)-96, DefaultValues.WINDOW_NAME, NULL, NULL);
        
      
		DefaultValues.window = glfwCreateWindow(DefaultValues.WIDTH, DefaultValues.HEIGHT, DefaultValues.WINDOW_NAME, NULL, NULL);

		glfwMakeContextCurrent(DefaultValues.window);
		//Activate VSync.
		glfwSwapInterval(GL_FALSE);
		
		// Get the resolution of the primary monitor
        DefaultValues.vidmode = glfwGetVideoMode(glfwGetPrimaryMonitor());
		
        // Center our window
//        glfwSetWindowPos(
//        		DefaultValues.window,
//            (8),
//            (32)
//        );
		 glfwSetWindowPos(
	        		DefaultValues.window,
	            (GLFWvidmode.width(DefaultValues.vidmode) - DefaultValues.WIDTH) / 2,
	            (GLFWvidmode.height(DefaultValues.vidmode) - DefaultValues.HEIGHT) / 2
	        );
		
		glfwShowWindow(DefaultValues.window);
		
		
        
		
		
		GLContext.createFromCurrent();
		
		//sceneLoad();
        
		World.generateWorld();
		
		Input.setInput(DefaultValues.window);
	}
	
	private static void sceneLoad(){
		// Set the clear color
        glClearColor(1.0f, 1.0f, 0.9f, 1.0f);
		//limpia colores y vertices de la ventana
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		//limpia eventos de la ventana
		glfwPollEvents();
		
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		glOrtho(DefaultValues.zoom, DefaultValues.WIDTH, DefaultValues.zoom, DefaultValues.HEIGHT, 1, -1);
		
		glMatrixMode(GL_MODELVIEW);
		glEnable(GL_TEXTURE_2D);
		
		
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		
		Texture.loadTexture("data/resources/textures/buttonStart/BUTTON.png").bind();
		glBegin(GL_QUADS);
		glColor3f(1.0f, 0.0f, 0.0f);
			//glTexCoord2d(0, 0);
			glVertex2d(0+320, 0-160);
			//glTexCoord2d(0, 1);
			glVertex2d(0+320, 0-160-480);
			//glTexCoord2d(1, 1);
			glVertex2d(0+320+320, 0-160-480);
			//glTexCoord2d(1, 0);
			glVertex2d(0+320+320, 0-160);
			
		glEnd();
		
		
		//refresca la pantalla
		glFlush();
		
		glfwSwapBuffers(DefaultValues.window);
		
		
		
	}
	
}
