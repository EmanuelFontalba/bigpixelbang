package game.window.scenesLoops;

import game.window.DefaultValues;
import game.window.Scenes;

import org.lwjgl.glfw.GLFWKeyCallback;

import util.Log;
import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.opengl.GL11.*;

public class Input {
	
	public static void setInput(long window){
		
		glfwSetKeyCallback(window, new GLFWKeyCallback(){
			
			@Override
			public void invoke(long window, int key, int arg2, int action, int arg4) {

				if(key == GLFW_KEY_KP_ADD && action == GLFW_RELEASE && DefaultValues.zoom < DefaultValues.ZOOM_MAX){

					DefaultValues.zoom+=50;
					glTranslated(25, 25, 0);
					

				}
				if(key == GLFW_KEY_KP_SUBTRACT && action == GLFW_RELEASE && DefaultValues.zoom > DefaultValues.ZOOM_MIN){

					DefaultValues.zoom-=50;
					glTranslated(-25, -25, 0);

				}
				if(key == GLFW_KEY_ESCAPE && action == GLFW_RELEASE && DefaultValues.scene == Scenes.GAME ){

					DefaultValues.scene = Scenes.MENU;
					Log.show("Go to the menu.");

				}
				
				if(key == GLFW_KEY_ENTER && action == GLFW_RELEASE && DefaultValues.scene == Scenes.MENU ){

					DefaultValues.scene = Scenes.GAME;
					Log.show("Go to the game.");

				}
			}
			
		});
		
	}
	
}
