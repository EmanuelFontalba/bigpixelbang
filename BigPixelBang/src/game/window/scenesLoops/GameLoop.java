package game.window.scenesLoops;

import static org.lwjgl.glfw.GLFW.glfwPollEvents;
import static org.lwjgl.glfw.GLFW.glfwSwapBuffers;
import static org.lwjgl.opengl.GL11.*;
import world.World;
import game.textures.Textures;
import game.window.DefaultValues;

public class GameLoop {
	
//	private static Chunk chunk = new Chunk(DefaultValues.WIDTH/2-32, DefaultValues.HEIGHT/2);
//	private static Chunk chunk2 = new Chunk(DefaultValues.WIDTH/2-32+512, DefaultValues.HEIGHT/2-256);
//	private static Chunk chunk3 = new Chunk(DefaultValues.WIDTH/2-32-512, DefaultValues.HEIGHT/2-256);
//	private static Chunk chunk4 = new Chunk(DefaultValues.WIDTH/2-32, DefaultValues.HEIGHT/2-512);
	
	public static void start(){
			
			//limpia colores y vertices de la ventana
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
			//limpia eventos de la ventana
			glfwPollEvents();
			
			initOpenGl();
				//Draw.createChunk(DefaultValues.WIDTH/2-32, DefaultValues.HEIGHT/2);
				//block.render();
				//Draw.createCube(200, 200);
				//chunk.render();
//				chunk2.render();
//				chunk3.render();
//				chunk4.render();
			
			World.renderWorld();
			
			//refresca la pantalla
			glFlush();
			glfwSwapBuffers(DefaultValues.window);
			
	}

	private static void initOpenGl() {
		
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		glOrtho(DefaultValues.zoom, DefaultValues.WIDTH, DefaultValues.zoom, DefaultValues.HEIGHT, 1, -1);
		//glTranslated(-300, -200, 0);
		
		glMatrixMode(GL_MODELVIEW);
		glEnable(GL_TEXTURE_2D);
		
		
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		
	}

}
