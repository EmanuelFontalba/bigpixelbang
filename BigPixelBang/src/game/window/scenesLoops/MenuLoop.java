package game.window.scenesLoops;

import static org.lwjgl.glfw.GLFW.glfwPollEvents;
import static org.lwjgl.glfw.GLFW.glfwSwapBuffers;
import static org.lwjgl.opengl.GL11.GL_BLEND;
import static org.lwjgl.opengl.GL11.GL_COLOR_BUFFER_BIT;
import static org.lwjgl.opengl.GL11.GL_DEPTH_BUFFER_BIT;
import static org.lwjgl.opengl.GL11.GL_MODELVIEW;
import static org.lwjgl.opengl.GL11.GL_ONE_MINUS_SRC_ALPHA;
import static org.lwjgl.opengl.GL11.GL_PROJECTION;
import static org.lwjgl.opengl.GL11.GL_SRC_ALPHA;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_2D;
import static org.lwjgl.opengl.GL11.glBlendFunc;
import static org.lwjgl.opengl.GL11.glClear;
import static org.lwjgl.opengl.GL11.glEnable;
import static org.lwjgl.opengl.GL11.glFlush;
import static org.lwjgl.opengl.GL11.glLoadIdentity;
import static org.lwjgl.opengl.GL11.glMatrixMode;
import static org.lwjgl.opengl.GL11.glOrtho;
import game.window.DefaultValues;
import util.Draw;

public class MenuLoop {

	public static void start(){

		

			//limpia colores y vertices de la ventana
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
			//limpia eventos de la ventana
			glfwPollEvents();

			initOpenGl();
				Draw.createQuad(200, 200);

			//refresca la pantalla
			glFlush();
			glfwSwapBuffers(DefaultValues.window);


	}

	private static void initOpenGl() {

		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		glOrtho(DefaultValues.zoom, DefaultValues.WIDTH, DefaultValues.zoom, DefaultValues.HEIGHT, 1, -1);
		//glTranslated(-300, -200, 0);
		glMatrixMode(GL_MODELVIEW);
		glEnable(GL_TEXTURE_2D);


		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	}

}
