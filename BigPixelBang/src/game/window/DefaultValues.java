package game.window;

import java.nio.ByteBuffer;

import util.Texture;
import game.window.scenesLoops.Scenes;


public class DefaultValues {
	
	public static final int WIDTH = 1024;
	public static final int HEIGHT = 768;
	public static final String WINDOW_NAME = "Big Pixel Bang";
	public static long window;
	public static final String PATH_TEXTURE = "data/resources/textures/";
	public static double zoom = 0;
	public static Scenes scene = Scenes.GAME;
	public static final double ZOOM_MAX = 400;
	public static final double ZOOM_MIN = -600;
	public static ByteBuffer vidmode;
	
	
}
